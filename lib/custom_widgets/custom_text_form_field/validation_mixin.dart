import 'package:app_builder/locale/app_localizations.dart';
import 'package:flutter/material.dart';



mixin ValidationMixin<T extends StatefulWidget> on State<T> {
 

  String? validateUserEmail(String userEmail) {
    if (userEmail.trim().isEmpty) {
      return AppLocalizations.of(context)!.translate('email_validation');
    }

    return null;
  }



  String? validateUserName(String userName) {
    if (userName.trim().isEmpty) {
      return AppLocalizations.of(context)!.translate('user_name_validation');
    }

    return null;
  }


}
