
import 'package:app_builder/utils/app_colors.dart';
import 'package:flutter/material.dart';






class PageContainer extends StatelessWidget {
  final Widget child;

  const PageContainer({Key? key, required this.child}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
        color:  mainAppColor,
        child: SafeArea(
          child: GestureDetector(
            onTap: () =>
              FocusScope.of(context).requestFocus( FocusNode())
            ,
            child: child,
          ),
        ));
  }
}
