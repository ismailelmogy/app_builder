import 'package:app_builder/custom_widgets/app_drawer/widgets/web_view.dart';
import 'package:app_builder/models/menu_item.dart';
import 'package:app_builder/custom_widgets/app_drawer/widgets/post_details.dart';
import 'package:app_builder/utils/app_colors.dart';
import 'package:flutter/material.dart';

class MenuItem extends StatelessWidget {
  final AnimationController? animationController;
  final Animation<double>? animation;
  final MenuItemModel? menuItemModel;
  final int? index;

  const MenuItem({
    Key? key,
    this.animationController,
    this.animation,
    this.menuItemModel,
    this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: animationController!,
        builder: (BuildContext context, Widget? child) {
          return FadeTransition(
              opacity: animation!,
              child: Transform(
                transform: Matrix4.translationValues(
                    100 * (1.0 - animation!.value), 0.0, 0.0),
                child: Container(
                  color: AppColors.listItemBg,
                  child: ListTile(
                    leading: const Icon(
                      Icons.edit,
                    ),
                    title: Text(menuItemModel!.title!),
                    onTap: () {
                      if (menuItemModel!.component! == 'posts') {
                        if (menuItemModel!.parameters!.userId != null) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PostDetailsScreen(
                                        index: index,
                                        title: menuItemModel!.title!,
                                      )));
                        } else {
                          Navigator.pop(context);
                        }
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => WebViewScreen(
                                      title: menuItemModel!.title,
                                      url: menuItemModel!.parameters!.url,
                                    )));
                      }
                    },
                  ),
                ),
              ));
        });
  }
}
