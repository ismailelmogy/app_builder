import 'package:app_builder/custom_widgets/color_loader/color_loader.dart';
import 'package:app_builder/custom_widgets/connectivity/network_indicator.dart';
import 'package:app_builder/custom_widgets/safe_area/page_container.dart';
import 'package:app_builder/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewScreen extends StatefulWidget {
  final String? title;
  final String? url;
  const WebViewScreen({Key? key, this.title, this.url}) : super(key: key);

  @override
  _WebViewScreenState createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> {
  bool _isLoading = false;
  @override
  Widget build(BuildContext context) {
    return NetworkIndicator(child: PageContainer(
      child: Scaffold(
          appBar: AppBar(
  backgroundColor: AppColors.headerBg,
            title: Text(
              widget.title!,
             
            ),
            centerTitle: true,
          ),
          body: Stack(
            children: [
              WebView(
                  initialUrl: widget.url,
                  javascriptMode: JavascriptMode.unrestricted,
                  onPageStarted: (start) {
                    setState(() {
                      _isLoading = true;
                    });
                  },
                  onPageFinished: (finish) {
                    setState(() {
                      _isLoading = false;
                    });
                  }),
              _isLoading
                  ? Center(
                 
                      child: SpinKitFadingCircle(color: AppColors.headerBg),
                    )
                  : Container()
            ],
          )),
    ));
  }
}
