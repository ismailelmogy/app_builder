import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:app_builder/custom_widgets/app_drawer/widgets/menu_item.dart';
import 'package:app_builder/custom_widgets/color_loader/color_loader.dart';
import 'package:app_builder/custom_widgets/no_data/no_data.dart';
import 'package:app_builder/locale/app_localizations.dart';
import 'package:app_builder/models/menu_item.dart';
import 'package:app_builder/providers/menu_items_provider.dart';
import 'package:app_builder/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app_builder/utils/error.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  State<AppDrawer> createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> with TickerProviderStateMixin {
  double _width = 0;
  AnimationController? _animationController;
  bool _initialRun = true;
  Future<List<MenuItemModel>>? _menItemList;
  MenuItemsProvider? _menuItemsProvider;

  @override
  void didChangeDependencies() async {
    if (_initialRun) {
      _menuItemsProvider = Provider.of<MenuItemsProvider>(context);
      _menItemList = _menuItemsProvider!.getMenuItemList();
      _initialRun = false;
    }
    super.didChangeDependencies();
  }

  @override
  void initState() {
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _animationController?.dispose();
    super.dispose();
  }

  Widget _buildAppDrawer(BuildContext context) {
    _width = MediaQuery.of(context).size.width;

    return  Theme(
           data: Theme.of(context).copyWith(
                 canvasColor: AppColors.headerBg
              ),
              child: Drawer(
          
          child: Column(
            children: [
              const SizedBox(height: 20,),

              Row(
                children:  [
                  const Expanded(child: SizedBox()),
                 Container(
                   margin: const EdgeInsets.symmetric(horizontal: 15),
                   child: InkWell(
                     child: const Icon(Icons.close,
                   color: Colors.white,),
                   onTap: ()=> Navigator.pop(context),

                   ),
                 )
                ],
              ),
               SizedBox(

                height: 130,
                child:    Center(
      child: DefaultTextStyle(
        
  style:  TextStyle(
    fontSize: 25.0,
    color: AppColors.headerText
  ),
  child: AnimatedTextKit(
    totalRepeatCount: 20,
    animatedTexts: [
      WavyAnimatedText('App Builder'),
      
    ],
    isRepeatingAnimation: true,
    onTap: () {
      print("Tap Event");
    },
  ),
),
    ),
              ),
              
              Expanded(
                child: FutureBuilder<List<MenuItemModel>>(
                    future: _menItemList,
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return const Center(
                            child: ColorLoader(
                              radius: 20.0,
                              dotRadius: 5.0,
                            ),
                          );
                        case ConnectionState.active:
                          return const Text('');
                        case ConnectionState.waiting:
                          return const Center(
                            child: ColorLoader(
                              radius: 20.0,
                              dotRadius: 5.0,
                            ),
                          );
                        case ConnectionState.done:
                          if (snapshot.hasError) {
                            return Error(
                                //  errorMessage: snapshot.error.toString(),
                                errorMessage: AppLocalizations.of(context)!
                                    .translate('error'));
                          } else {
                            return snapshot.data!.isNotEmpty
                                ? ListView.separated(
                                    itemCount: snapshot.data!.length,
                                    separatorBuilder: (context,index){
                                      return const Divider(
                                        height: 10,
                                        color: Colors.grey,
                                      );
                                    },
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      var count = snapshot.data!.length;
                                      var animation =
                                          Tween(begin: 0.0, end: 1.0).animate(
                                        CurvedAnimation(
                                          parent: _animationController!,
                                          curve: Interval(
                                              (1 / count) * index, 1.0,
                                              curve: Curves.fastOutSlowIn),
                                        ),
                                      );
                                      _animationController?.forward();

                                      return MenuItem(
                                        index: index,
                                        menuItemModel: snapshot.data![index],
                                        animation: animation,
                                        animationController:
                                            _animationController,
                                      );
                                    },
                               
                                  )
                                : const Center(
                                    child: NoData(message: 'No Data'),
                                  );
                          }
                      }
                    }),
              ),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: _buildAppDrawer(context));
  }
}
