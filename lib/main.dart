import 'package:app_builder/locale/app_localizations.dart';
import 'package:app_builder/providers/posts_provider.dart';
import 'package:app_builder/providers/menu_items_provider.dart';
import 'package:app_builder/ui/home/home.dart';
import 'package:app_builder/utils/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'theme/style.dart';
import 'utils/app_config.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
   runApp(const MyApp());

}



class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    AppConfig.getAppConfigurations(context);
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => MenuItemsProvider(),
          ),
          ChangeNotifierProvider(
            create: (_) => PostsProvider(),
          ),
        ],
        child: MaterialApp(
          title: 'App Builder',
          locale: const Locale('en', 'US'),
          supportedLocales: const [
            Locale('en', 'US'),
            Locale('ar', 'SA'),
          ],
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate
          ],
          routes: routes,
          theme: materialThemeData(),
          debugShowCheckedModeBanner: false,
        ));
  }
}
