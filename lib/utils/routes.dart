
import 'package:app_builder/ui/home/home.dart';
import 'package:app_builder/ui/splash/splash.dart';

final routes = {
  '/': (context) => const SplashScreen(),
  '/home_screen' : (context)=> const HomeScreen()

};
