

import 'package:flutter/cupertino.dart';

import 'hex_color.dart';

final mainAppColor = HexColor('1C608D');
final hintColor = HexColor('A2A2A2');
final accentColor = HexColor('94C24B');


class AppColors {
    static  Color? pageBg ;
    static Color? headerBg;
    static Color? headerText;
    static Color? menuBg;
    static Color? menuItemBGColor;
    static Color? menuItemSelectedBgColor;
    static Color? listTitle;
    static Color? listItemBg;
    static Color? textColor; 

}



