import 'dart:convert';
import 'package:app_builder/providers/menu_items_provider.dart';
import 'package:app_builder/utils/app_colors.dart';
import 'package:app_builder/utils/hex_color.dart';
import 'package:app_builder/utils/urls.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class AppConfig {
  final String apiUrl;

  AppConfig({required this.apiUrl});

   

  static void getAppConfigurations(BuildContext context) async {
    // load the json file
    final contents = await rootBundle.loadString(
      'assets/config/config.json',
    );

    // decode our json
    final json = jsonDecode(contents);

    // convert our JSON into an instance of our AppConfig class
    Urls.BASE_URL = json['mainConfig']['baseUrl'];

    AppColors.pageBg = HexColor(json['appColor']['pageBg']);
    AppColors.headerBg = HexColor(json['appColor']['headerBg']);
    AppColors.headerText = HexColor(json['appColor']['headerText']);
    AppColors.menuBg = HexColor(json['appColor']['menuBg']);
    AppColors.menuItemBGColor = HexColor(json['appColor']['menuItemBGColor']);
    AppColors.menuItemSelectedBgColor =
        HexColor(json['appColor']['menuItemSelectedBgColor']);
    AppColors.listTitle = HexColor(json['appColor']['listTitle']);
    AppColors.listItemBg = HexColor(json['appColor']['listItemBg']);
    AppColors.textColor = HexColor(json['appColor']['textColor']);

    
  }
}
