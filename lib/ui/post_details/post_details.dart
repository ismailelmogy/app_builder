import 'package:app_builder/custom_widgets/color_loader/color_loader.dart';
import 'package:app_builder/custom_widgets/connectivity/network_indicator.dart';
import 'package:app_builder/custom_widgets/safe_area/page_container.dart';
import 'package:app_builder/locale/app_localizations.dart';
import 'package:app_builder/models/post.dart';
import 'package:app_builder/providers/menu_items_provider.dart';
import 'package:app_builder/providers/posts_provider.dart';
import 'package:app_builder/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app_builder/utils/error.dart';

class PostDetailsScreen extends StatefulWidget {
  final int? postId;
  final String? title;
  const PostDetailsScreen({Key? key, this.postId, this.title}) : super(key: key);

  @override
  _PostDetailsScreenState createState() => _PostDetailsScreenState();
}

class _PostDetailsScreenState extends State<PostDetailsScreen> {
  bool _initialRun = true;
  late Future<Post> _postDetails;

  late PostsProvider _postsProvider;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (_initialRun) {
      _postsProvider = Provider.of<PostsProvider>(context);
      _postDetails = _postsProvider.getPostDetails(widget.postId!);
      _initialRun = false;
    }
  }

  Widget _buildBodyItem() {
    return FutureBuilder<Post>(
        future: _postDetails,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return const Center(
                child: ColorLoader(
                  radius: 20.0,
                  dotRadius: 5.0,
                ),
              );
            case ConnectionState.active:
              return const Text('');
            case ConnectionState.waiting:
              return const Center(
                child: ColorLoader(
                  radius: 20.0,
                  dotRadius: 5.0,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                return Error(
                 // errorMessage: snapshot.error.toString(),
                  errorMessage: AppLocalizations.of(context)!
                      .translate('error'
                      )
                );
              } else {
                return ListView(
                  children: [
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        snapshot.data!.title!,
                        style:
                            TextStyle(fontSize: 18, color: AppColors.textColor),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          snapshot.data!.body!,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              fontSize: 15, color: mainAppColor),
                        ))
                  ],
                );
              }
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      backgroundColor: AppColors.headerBg,
      title: Text(widget.title!),
    );
    return NetworkIndicator(
      child: PageContainer(
          child: Scaffold(
        appBar: appBar,
        body: _buildBodyItem(),
      )),
    );
  }
}
