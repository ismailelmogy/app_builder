
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:app_builder/custom_widgets/safe_area/page_container.dart';
import 'package:app_builder/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

    Future initData() async {
    await Future.delayed(const Duration(seconds: 3));
  }
  
 @override
  void initState() {
      super.initState();
   
    initData().then((value) =>     Navigator.pushReplacementNamed(context,    '/home_screen'));
  
  }

  Widget _buildBodyItem() {
    return Center(
      child: DefaultTextStyle(
  style:  TextStyle(
    fontSize: 30.0,
    color: AppColors.headerBg
  ),
  child: AnimatedTextKit(
    animatedTexts: [
      WavyAnimatedText('App Builder'),
      
    ],
    isRepeatingAnimation: true,
    onTap: () {
      print("Tap Event");
    },
  ),
),
    );
    // return Image.asset(
    //   'assets/images/splash.png',
    //   fit: BoxFit.cover,
    //   height: double.infinity,
    //   width: double.infinity,
    //   alignment: Alignment.center,
    // );
  }

  @override
  Widget build(BuildContext context) {
    return PageContainer(child:  Scaffold(
   
      body: _buildBodyItem(),
    ));
  }
}
