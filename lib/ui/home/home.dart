import 'package:app_builder/custom_widgets/app_drawer/app_drawer.dart';
import 'package:app_builder/custom_widgets/color_loader/color_loader.dart';
import 'package:app_builder/custom_widgets/connectivity/network_indicator.dart';
import 'package:app_builder/custom_widgets/no_data/no_data.dart';
import 'package:app_builder/custom_widgets/safe_area/page_container.dart';
import 'package:app_builder/locale/app_localizations.dart';
import 'package:app_builder/models/post.dart';
import 'package:app_builder/providers/posts_provider.dart';
import 'package:app_builder/ui/home/widgets/post_item.dart';
import 'package:app_builder/ui/post_details/post_details.dart';
import 'package:app_builder/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app_builder/utils/error.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({ Key? key }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>    
with TickerProviderStateMixin {

  AnimationController? _animationController;
  bool _initialRun = true;
  Future<List<Post>>? _postList;
  PostsProvider? _postsProvider;

  @override
  void didChangeDependencies() async {
    if (_initialRun) {
      _postsProvider = Provider.of<PostsProvider>(context);
      _postList = _postsProvider!.getPostList();
      _initialRun = false;
    }
    super.didChangeDependencies();
  }

  @override
  void initState() {
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _animationController?.dispose();
    super.dispose();
  }
  
  final GlobalKey<ScaffoldState> _scaffoldKey =  GlobalKey<ScaffoldState>();

  Widget  _buildBodyItem(){
    return FutureBuilder<List<Post>>(
                    future: _postList,
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return const Center(
                            child: ColorLoader(
                              radius: 20.0,
                              dotRadius: 5.0,
                            ),
                          );
                        case ConnectionState.active:
                          return const Text('');
                        case ConnectionState.waiting:
                          return const Center(
                            child: ColorLoader(
                              radius: 20.0,
                              dotRadius: 5.0,
                            ),
                          );
                        case ConnectionState.done:
                          if (snapshot.hasError) {
                            return Error(
                                //  errorMessage: snapshot.error.toString(),
                                errorMessage: AppLocalizations.of(context)!
                                    .translate('error'));
                          } else {
                            return snapshot.data!.isNotEmpty
                                ? ListView.builder(
                                    itemCount: snapshot.data!.length,
                                  
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                          var count = snapshot.data!.length > 10
                      ? 10
                      : snapshot.data!.length;
                      var animation = Tween(begin: 0.0, end: 1.0).animate(
                        CurvedAnimation(
                          parent: _animationController!,
                          curve: Interval((1 / count) * index, 1.0,
                              curve: Curves.fastOutSlowIn),
                        ),
                      );
                
                  _animationController?.forward();

                                      return InkWell(
                                        onTap: ()=> Navigator.push(context, MaterialPageRoute(builder: (context)=>PostDetailsScreen(
                                          postId: snapshot.data![index].id,
                                          title: snapshot.data![index].title,
                                        ))),
                                        child: PostItem(
                                          
                                          post: snapshot.data![index],
                                          animation: animation,
                                          animationController:
                                              _animationController,
                                        ),
                                      );
                                    },
                               
                                  )
                                : const Center(
                                    child: NoData(message: 'No Data'),
                                  );
                          }
                      }
                    });
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      backgroundColor: AppColors.headerBg,
      title: const Text('Home Screen'),
      leading:  IconButton(
        icon:  const Icon(Icons.menu),
        onPressed:  () =>       _scaffoldKey.currentState!.openDrawer(),
      
      ),
    );
    return NetworkIndicator(
      child: PageContainer(child: Scaffold(
        drawer: const AppDrawer(),
        key: _scaffoldKey,
        appBar: appBar,
        body: _buildBodyItem(),
      )),
      
    );
  }
}

