import 'package:app_builder/models/post.dart';
import 'package:app_builder/utils/app_colors.dart';
import 'package:flutter/material.dart';

class PostItem extends StatelessWidget {
  final AnimationController? animationController;
  final Animation<double>? animation;
  final Post? post;
  const PostItem({
    Key? key,
    this.animationController,
    this.animation,
    this.post,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: animationController!,
        builder: (BuildContext context, Widget? child) {
          return FadeTransition(
              opacity: animation!,
              child: Transform(
                  transform: Matrix4.translationValues(
                      0.0, 50 * (1.0 - animation!.value), 0.0),
                  child: Container(
                    padding:const EdgeInsets.symmetric(horizontal: 15),
                    margin: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width * 0.02,
                        vertical: 10),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey[300]!),
                      color: Colors.white,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    child: Column(
                      children: [Text(post!.title!,
                      style: TextStyle(
                        color: AppColors.listTitle,
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                      ),), Text(post!.body!,
                       style: TextStyle(
                        color: AppColors.textColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                      ))],
                    ),
                  )));
        });
  }
}
