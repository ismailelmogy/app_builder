import 'package:app_builder/utils/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

ThemeData materialThemeData() {
  return ThemeData(
      primaryColor: mainAppColor,
      hintColor: hintColor,
      brightness: Brightness.light,
      scaffoldBackgroundColor: const Color(0xffFFFFFF),
      fontFamily: 'Fairuz',
      platform: TargetPlatform.iOS,
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: mainAppColor,
      ),
      textTheme: TextTheme(
        // app bar style //  listTile (profile)
        headline1: const TextStyle(
            color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),

        //  listTile (drawer)
        headline2: const TextStyle(
            color: Colors.black, fontSize: 14.0, fontWeight: FontWeight.bold),

        // hint style of text form
        headline3: TextStyle(
            color: hintColor, fontSize: 14, fontWeight: FontWeight.w400),

        button: const TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16.0),

        headline4: const TextStyle(
            color: Color(0xff4C5264),
            fontWeight: FontWeight.bold,
            fontSize: 12.0),
      ));
}

CupertinoThemeData cupertinoThemeData() {
  return CupertinoThemeData(
      primaryColor: mainAppColor,
      brightness: Brightness.light,
      primaryContrastingColor: accentColor,
      scaffoldBackgroundColor: const Color(0xffFFFFFF),
      textTheme: CupertinoTextThemeData(
        navActionTextStyle: const TextStyle(
            color: Color(0xff7F2C2D),
            fontSize: 16,
            fontWeight: FontWeight.bold),
        textStyle: TextStyle(
            color: hintColor, fontSize: 14, fontWeight: FontWeight.w400),
        actionTextStyle: const TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14.0),
      ));
}
