

class User {
  User({
    this.token,
    this.id,
    this.username,
    this.email,
    this.phone,
    this.address,
  
    this.userImg,
 
  });

  String? token;
  int? id;
  String? username;
  String? email;
  String? phone;
  String? address;
 
  String? userImg;


  factory User.fromJson(Map<String, dynamic> json) => User(
        token: json["token"],
        id: json["id"],
        username: json["username"],
        email: json["email"],
        phone: json["phone"],
        address: json["address"],
       
        userImg: json["user_img"],
    
      );

  Map<String, dynamic> toJson() => {
        "token": token,
        "id": id,
        "username": username,
        "email": email,
        "phone": phone,
        "address": address,
      
        "user_img": userImg,
    
      };
}
