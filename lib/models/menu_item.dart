
class MenuItemModel {
    MenuItemModel({
        this.component,
        this.parameters,
        this.title,
    });

    String? component;
    Parameters? parameters;
    String? title;

    factory MenuItemModel.fromJson(Map<String, dynamic> json) => MenuItemModel(
        component: json["component"],
        parameters: Parameters.fromJson(json["parameters"]),
        title: json["title"],
    );

    Map<String, dynamic> toJson() => {
        "component": component,
        "parameters": parameters!.toJson(),
        "title": title,
    };
}

class Parameters {
    Parameters({
        this.apiName,
        this.userId,
        this.url,
    });

    String? apiName;
    int? userId;
    String? url;

    factory Parameters.fromJson(Map<String, dynamic> json) => Parameters(
        apiName: json["apiName"],
        userId: json["userId"],
        url: json["url"],
    );

    Map<String, dynamic> toJson() => {
        "apiName": apiName,
        "userId": userId,
        "url": url,
    };
}
