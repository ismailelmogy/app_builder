import 'package:app_builder/models/post.dart';
import 'package:app_builder/networking/api_provider.dart';
import 'package:app_builder/utils/urls.dart';
import 'package:flutter/material.dart';

class PostsProvider extends ChangeNotifier {
  final ApiProvider _apiProvider = ApiProvider();

  Future<List<Post>> getPostList() async {
    final response = await _apiProvider.get(
      Urls.POSTS_URL,
    );

    List<Post> postList = [];
    if (response['status'] == 200) {
      Iterable iterable = response['response'];
      postList = iterable.map((model) => Post.fromJson(model)).toList();
    }

    return postList;
  }

  Future<Post> getPostDetails(int postId) async {
    Post post = Post();
    final response = await _apiProvider.get(
      Urls.POSTS_URL + "/" + postId.toString(),
    );

    if (response['status'] == 200) {
      post = Post.fromJson(response['response']);
    }

    return post;
  }
}
