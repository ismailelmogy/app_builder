import 'dart:convert';

import 'package:app_builder/models/menu_item.dart';
import 'package:app_builder/models/post.dart';
import 'package:app_builder/networking/api_provider.dart';
import 'package:app_builder/utils/urls.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MenuItemsProvider extends ChangeNotifier {

  final ApiProvider _apiProvider = ApiProvider(); 
  List<MenuItemModel> _menuItemList = [];

  List<MenuItemModel> get menuItemList => _menuItemList;

  Future<List<MenuItemModel>> getMenuItemList() async {
    final contents = await rootBundle.loadString(
      'assets/config/config.json',
    );

    // decode our json
    final json = jsonDecode(contents);

    // convert our JSON into an instance of our AppConfig class
    Iterable iterable = json['menuItems'];
    _menuItemList =
        iterable.map((model) => MenuItemModel.fromJson(model)).toList();

    return _menuItemList;
  }

  Future<Post> getPostDetails(int index) async {
  Post post = Post();
    final response = await _apiProvider.get(Urls.BASE_URL + "/" + _menuItemList[index].parameters!.apiName!  + "/" + _menuItemList[index].parameters!.userId.toString(),
      );

    if (response['status'] == 200) {
      post = Post.fromJson(response['response']);
    }
    
    
     

    return post;
  
  }
}
